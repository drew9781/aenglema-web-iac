variable "pvt_key" {}

terraform {
  backend "s3" {
    bucket="aenglema-terraform"
    key="terraform/prod/aenglema-web/terraform.tfstate"
    region="us-east-1"
  }
}

provider "aws" {
  region   = "us-east-1"
}

resource "aws_lightsail_static_ip_attachment" "app" {
  static_ip_name = aws_lightsail_static_ip.app.name
  instance_name  = aws_lightsail_instance.app.name
}

resource "aws_lightsail_static_ip" "app" {
  name = "aenglema-web"
}



resource "aws_lightsail_instance" "app" {
  name              = "aenglema-web.aenglema.com"
  availability_zone = "us-east-1b"
  blueprint_id      = "debian_10"
  bundle_id         = "nano_2_0"
  key_pair_name     = "aws"

  provisioner "remote-exec" {
    inline = ["sudo apt update", "sudo apt install python3 -y", "echo Done!"]

    connection {
      host        = self.public_ip_address
      type        = "ssh"
      user        = "admin"
      private_key = file(var.pvt_key)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.public_ip_address},' --private-key ${var.pvt_key}  aenglema_web_install.yml"
  }
}

data "aws_route53_zone" "zone" {
  name         = "aenglema.com."
}

resource "aws_route53_record" "app" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "blog.${data.aws_route53_zone.zone.name}"
  type    = "A"
  ttl     = "300"
  records = [aws_lightsail_static_ip.app.ip_address]
}
resource "aws_route53_record" "app-cname" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "www.${data.aws_route53_zone.zone.name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["blog.aenglema.com"]
}
resource "aws_route53_record" "app-cname2" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "resume.${data.aws_route53_zone.zone.name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["blog.aenglema.com"]
}